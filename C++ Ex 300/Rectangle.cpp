#include <iostream>
#include "Rectangle.h"

// Constructeur par d�faut
Rectangle::Rectangle()
    : m_largeur(0), m_longueur(0)
{

}

// Costructeur surcharg�
Rectangle::Rectangle(unsigned int _largeur, unsigned int _longueur)
    : m_largeur(_largeur), m_longueur(_longueur)
{

}

// Destructeur
Rectangle::~Rectangle()
{

}

// M�thode calcul perimetre
unsigned int Rectangle::perimetre()const
{
    return(2*getLargeur()+2*getLongueur());
}

// M�thode calcul aire
unsigned int Rectangle::aire()const
{
    return(getLargeur()*getLongueur());
}

// M�thode pour afficher info
void Rectangle::affichage()const
{
    std::cout << "Informations du rectangle :" << std::endl << std::endl ;
    std::cout << "Longueur :" << m_longueur << std::endl ;
    std::cout << "Largeur :" << m_largeur << std::endl ;
    std::cout << "P�rim�tre :" << perimetre() << std::endl ;
    std::cout << "Aire :" << aire() << std::endl ;
}

// Accesseurs
unsigned int Rectangle::getLargeur()const
{
    return(m_largeur);
}

unsigned int Rectangle::getLongueur()const
{
    return(m_longueur);
}

unsigned int Rectangle::setLargeur(unsigned int _largeur)const
{
    m_largeur=_largeur;
}

unsigned int Rectangle::setLongueur(unsigned int _longueur)const
{
    m_longueur=_longueur;
}
