#ifndef RECTANGLE_H_INCLUDED
#define RECTANGLE_H_INCLUDED

///Coucou �a va ? C'est Delphine !

class Rectangle
{
    private:
        unsigned int m_longueur;                             // Attributs caract�risants le rectangle
        unsigned int m_largeur;

    public:
        Rectangle();                                // Constructeur par d�faut
        Rectangle(unsigned int _largeur, unsigned int _longueur);     // Constructeur surcharg�
        ~Rectangle();                               // Destructeur

        unsigned int perimetre()const;                       // M�thode calcul perimetre
        unsigned int aire()const;                            // M�thode calcul aire
        void affichage()const;                      // M�thode pour afficher info

        unsigned int getLargeur()const;             // Accesseurs
        unsigned int getLongueur()const;
        unsigned int setLargeur(unsigned int _largeur)const;
        unsigned int setLongueur(unsigned int _longueur)const;
};

#endif // RECTANGLE_H_INCLUDED
